
% Starting the server
% Bibliotecas 
:- use_module(library(http/thread_httpd)).
:- use_module(library(http/http_dispatch)).
:- use_module(library(http/http_json)).
:- use_module(library(http/http_files)).

server(Port) :-
   http_server(http_dispatch,[port(Port)]).

% A base de conhecimento é consultada de cada vez que se pretende executar o algoritmo.
% O ficheiro é alterado pela gestão de fornecedores, enviando os dados de todas as farmácias de cada vez que seja necessário verificar o caminho mais curto.
% farmacia(nome,lat,long,horario). 

consult_file(_Request):- consult(base).

:- http_handler('/consult',consult_file,[]).
:- http_handler('/path',handle,[]).

handle(Request) :-
   http_read_json(Request, DictIn,[json_object(dict)]),
   format(user_output,"~p~n",[Request]),
   format(user_output,"DictIn is: ~p~n",[DictIn]),
   DictOut=DictIn,
   reply_json(DictOut).




%  dist_farmacias(mafalda,ramos,D).
dist_farmacias(C1,C2,Dist):-
    farmacia(C1,Lat1,Lon1,_),
    farmacia(C2,Lat2,Lon2,_),
    distance(Lat1,Lon1,Lat2,Lon2,Dist).
	
degrees2radians(Deg,Rad):-
	Rad is Deg*0.0174532925.

% distance(latitude_first_point,longitude_first_point,latitude_second_point,longitude_second_point,distance
% in meters)
distance(Lat1, Lon1, Lat2, Lon2, Dis2):-
	degrees2radians(Lat1,Psi1),
	degrees2radians(Lat2,Psi2),
	DifLat is Lat2-Lat1,
	DifLon is Lon2-Lon1,
	degrees2radians(DifLat,DeltaPsi),
	degrees2radians(DifLon,DeltaLambda),
	A is sin(DeltaPsi/2)*sin(DeltaPsi/2)+ cos(Psi1)*cos(Psi2)*sin(DeltaLambda/2)*sin(DeltaLambda/2),
	C is 2*atan2(sqrt(A),sqrt(1-A)),
	Dis1 is 6371000*C,
	Dis2 is round(Dis1).



tsp2(Orig,Cam,N):-		findall(X,farmacia(X,_,_,_),L),
						length(L,N),
						vmp(Orig,[Orig],Orig,Cam,N),!,
						tell('path.txt'),
						write(Cam),
						told.


vmp(Orig,LA,_,Cam,1):-append([Orig],LA,Cam).
vmp(Orig,LA,Act,Cam,N):-N>0,
						findall((D,C),((dist_farmacias(Act,C,D),\+member(C,LA))),L), 
						sort(L,[(_,Dest)|_]),
						N1 is N-1,
						vmp(Orig,[Dest|LA],Dest,Cam,N1).